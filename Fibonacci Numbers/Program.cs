﻿using System;

namespace Fibonacci_Numbers
{
    class Program
    {
        static void Main(string[] args)
        {

            int prev = 0;
            int current = 1;
            int newNumber = 0;
            int total = 0;

            while (true)
            {
                newNumber = current + prev;
                prev = current;
                current = newNumber;
                if (current >= 4000000)
                {
                    break;
                }
                if(current % 2 == 0)
                {
                    total += current;
                }
            }

            Console.WriteLine(total);


        }
    }
}
